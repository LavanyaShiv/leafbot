package com.autoBot.testcases;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TC002_CreateLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC002_CreateLead";
		testcaseDec = "Login into leaftaps";
		author = "koushik";
		category = "smoke";
		excelFileName="TC002_CreateLead";
		
	}

	@Test(dataProvider="fetchData")
	public void runCreateLead(String userName,String password,String companyName,String firstName,String lastName) {
		new LoginPage()
		.enterUserName(userName)
		.enterPassWord(password)
		.clickLogin()
		.clickCrmsfa()
		.clickLeadTab()
		.clickCreateLeadLink()
		.enterCompanyName(companyName)
		.enterFirstName(firstName)
		.enterLastName(lastName)
		.clickCreateLeadButton()
		.verifyCreatedName(firstName);
		
	}
}
