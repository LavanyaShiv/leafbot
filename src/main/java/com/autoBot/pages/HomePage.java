package com.autoBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import com.autoBot.pages.HomePage;
import com.autoBot.pages.MyHomePage;

import com.autoBot.testng.api.base.Annotations;

public class HomePage extends Annotations{ 

	public HomePage() {
       PageFactory.initElements(driver, this);
	} 

//	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogout;
	public LoginPage clickLogout() {
		WebElement eleLogout = locateElement("class", "decorativeSubmit");
		//click(eleLogout);  
		return new LoginPage();
	}

	public HomePage verifyLoginName() {
		
		if(driver.findElementByTagName("h2").getText().contains("Demo sales manager")) {
			System.out.println("Login Verified");
		}
		else
		{
			System.out.println("Login Mismatch");
		}
		return this;
		
	}
	
	public MyHomePage clickCrmsfa()
	{
		driver.findElementByLinkText("CRM/SFA").click();
		return new MyHomePage();


	}
}







