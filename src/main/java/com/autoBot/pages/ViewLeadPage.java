package com.autoBot.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;


import com.autoBot.testng.api.base.Annotations;

public class ViewLeadPage extends Annotations{

	public ViewLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
@FindBy(how=How.ID,using="viewLead_firstName_sp")WebElement eleFirstName;
@FindBy(how=How.XPATH,using="//a[text()='Edit']")WebElement eleEditButton;
@FindBy(how=How.ID,using="viewLead_companyName_sp")WebElement eleCompanyName;
@FindBy(how=How.XPATH,using="//div[text()='View Lead']")WebElement eleViewLead;
@FindBy(how=How.LINK_TEXT,using="Duplicate Lead")WebElement eleDuplicateLeadButton;
@FindBy(how=How.ID,using="viewLead_firstName_sp")WebElement eleViewFirstName;
@FindBy(how=How.ID,using="viewLead_lastName_sp")WebElement eleViewLastName;
@FindBy(how=How.LINK_TEXT,using="Delete")WebElement eleDeleteButton;
@FindBy(how=How.LINK_TEXT,using="Find Leads")WebElement eleFindLeads;
	
	public ViewLeadPage verifyCreatedName(String data) {
		
		//if(eleFirstName.getText().contains(data))
				if(getElementText(eleFirstName).contains(data))
		 {
			System.out.println("Testcase Created Succesfully");
		}
		else
		{
			System.out.println("Name Mismatch");
		}
		return this;		
	}
	
	/*public UpdatePage clickEditButton() {
		eleEditButton.click();
		return new UpdatePage();
	}
	*/
	public ViewLeadPage verifyUpdatedCompanyName(String data) {		
		String newCompanyName = eleCompanyName.getText();
		String[] companyName = newCompanyName.split("\\s");
		for (String name : companyName) {
			System.out.println("Company Name : "+name);
			if (name.equals(data)) {
				System.out.println("The updated company name appears correct after editing");
			} else
				System.out.println("The company name is not updated");
			break;
		}
		return this;		
	}

	public ViewLeadPage verifyPageTitle() {
		String title = eleViewLead.getText();
		if (title.equals("View Lead")) 
			System.out.println("View Lead Page Title is verified in Edit Lead Page");
		else
			System.out.println("Invalid title page");
		return this;
	}
	
	/*public DuplicateLeadPage clickDuplicateLeadButton() {
		eleDuplicateLeadButton.click();
		return new DuplicateLeadPage();
	}
	public ViewLeadPage verifyDuplicatedLeadName() {
		String duplicateFirstName = eleViewFirstName.getText();
		String duplicLastName = eleViewLastName.getText();
		String duplicateName = duplicateFirstName.concat(duplicLastName);
		new FindLeadsPage();
		String name=FindLeadsPage.name;
		if (name.equals(duplicateName))
			System.out.println("The Duplicated lead name is same as captured name");
		else
			System.out.println("The Duplicated lead name mismatches with captured name");
		return this;
	}
	
	public MyLeadPage clickDeleteButton() {
		eleDeleteButton.click();
		return new MyLeadPage();
	}
	
	public FindLeadsPage clickFindLeadsLink() {
		eleFindLeads.click();
		return new FindLeadsPage();
	}*/
	
}